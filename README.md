PiRemoteControl
===============

Raspberry Pi remote control with GPIO and GTK+

PiServer
========
Server programs for remote control with client


PiControl
=========
Client program for control remote server
